import configparser
import logging

import telegram
from flask import Flask, request
from telegram.ext import Dispatcher, MessageHandler, Filters, CommandHandler
# from touhou_ptt import UpdateTouhou,Touhou


# Load data from config.ini file
config = configparser.ConfigParser()
config.read('config.ini')

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

# Initial Flask app
app = Flask(__name__)

# Initial bot by Telegram access token
bot = telegram.Bot(token=(config['TELEGRAM']['ACCESS_TOKEN']))


@app.route('/hook', methods=['POST'])
def webhook_handler():
    """Set route /hook with POST method will trigger this method."""
    if request.method == "POST":
        update = telegram.Update.de_json(request.get_json(force=True), bot)

        # Update dispatcher process that handler to process this message
        dispatcher.process_update(update)
    return 'ok'


def reply_handler(bot,update):
    """Reply message."""
    text = update.message.text
    update.message.reply_text(text)
def start_handler(bot,update):
    bot.send_message(chat_id = update.message.chat_id,text = "喵~~喵~")
def updateTouhou(bot, update):
    bot.sendMessage(update.message.chat_id, text='start to update database')
    try:
        UpdateTouhou()
    except:
        bot.sendMessage(update.message.chat_id, text='I am sorry, but my ip is ban, please wait and try it again')
        return
    bot.sendMessage(update.message.chat_id, text='update touhouPtt finish')

# New a dispatcher for bot
dispatcher = Dispatcher(bot, None)

# Add handler for handling message, there are many kinds of message. For this handler, it particular handle text
# message.
# commandHandler
dispatcher.add_handler(MessageHandler(Filters.text, reply_handler))
dispatcher.add_handler(CommandHandler("start",start_handler))
# dispatcher.add_handler(CommandHandler("touhou", Touhou))
# dispatcher.add_handler(CommandHandler("Utouhou", updateTouhou))


if __name__ == "__main__":
    # Running server
    app.run(debug=True)